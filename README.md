# ARCHIVED
People still keep using this link, use [wos-manager](https://gitlab.com/vim_disel/wos-manager) instead. This was a fork and was merged already.
# This is an QT application for the hit game "Waste of Space" on roblox

used for querying the universe database via filters and colors

![image](res/images/thumbnail.png "Thumbnail")

# Overview

## Color matching

To match color you will have to click the "Main color" or the "Secondary color" checkbox, the checkboxes decide whether the color will be taken into the query. \
Quering with any color will rank the results with the top results being the closest to the desired color. \
If no color matching is enabled then the results will be ordered by their cords

## Filters

In the lower right part of the UI you have a list of filters, each filter has a checkbox and a label. \
If the checkbox is checked then the filter will be taken into account.

![image](res/images/filters.png "Filters")

in the image above you can see that the checkbox to the left of "Star type" is disabled, which means that planets arent filtered by their star type.
however the RingType filter is enabled and has the `IN` operator selected, which means that only planets with the selected types will show up in the query. In this case only planets with Ice or Stone rings will show. \
Stellar X filter is also enabled and has the `=` operator chosen, this means that only planets with their first Stellar X being equal to 48 will show up. \
When there are multiple Filters enabled then all of them have to be satisifed for a planet to show up in the output. in this case only planets that have their Stellar X equal to 48 ***and*** have either ice or stone rings

String have an extra `LIKE` operator for more refined searching, this operator is exactly the same as the SQL `Like` operator.
if you want to learn more about it then click [here](res/https://www.w3schools.com/mysql/mysql_like.asp)

# Setup

both the installer and the GNU/Linux compiled version is avaiable at
[**RELEASES**](res/https://gitlab.com/vim_disel/wos-guide-qt/-/releases/1.0.0)

if you dont want to use the provided binary/installer then you will need the following dependencies to run the script directly
- [python3](https://www.python.org/downloads/) (duh)
- [PySide6](https://pypi.org/project/PySide6/)
- sqlite3 (should be already installed)

you can also build the app yourself with pyinstaller by running the the command in the project root
```
pyinstaller -w --add-data "res:res" --add-data "LICENSE:." src/main.py
```
